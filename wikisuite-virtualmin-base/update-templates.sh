#!/bin/bash

# Expects a TEMPLATE variable to available with the template to clean and will
# generate the output in a variable called TEMPLATE_CLEAN
clean_template_for_virtualmin () {
  if [ -z "${TEMPLATE}" ] ; then
    echo "Empty TEMPLATE, when calling clean_template_for_virtualmin. Stopping progressing..."
    exit 1
  fi
  # Remove comments and empty lines
  # Replace tabs with spaces as tab is used to denote new line in the template
  # Finally convert new lines to tabs to add to the template
  TEMPLATE_CLEAN=$( echo "${TEMPLATE}" | sed -e 's/#.*$//' -e '/^ *$/d' -e 's/\t/    /g' | tr '\n' '\t')
}

update_template () {
  local FILE="$1"
  local DIRECTIVE="$2"
  local TEMPLATE="$3"

  grep -qE "^${DIRECTIVE}=" "${FILE}" || echo "${DIRECTIVE}=" >> "${FILE}"

  local CONTENT=$(cat ${FILE})
  TEMPLATE=$(echo "${TEMPLATE}" | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/\$/\\$/g' -e 's/&/\\\&/g')
  echo "${CONTENT}" | sed -e "s/^${DIRECTIVE}=.*$/${DIRECTIVE}=${TEMPLATE}/" > "${FILE}"
}

# Make sure cwd is the same folder as the script.
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1

TEMPLATE=$(cat assets/apache-template)
clean_template_for_virtualmin
APACHE_TEMPLATE="${TEMPLATE_CLEAN}"

TEMPLATE=$(cat assets/nginx-template)
clean_template_for_virtualmin
NGINX_TEMPLATE="${TEMPLATE_CLEAN}"

for T in assets/templates/* ; do
  update_template "${T}" "web" "${APACHE_TEMPLATE}"
  update_template "${T}" "virtualmin-nginx" "${NGINX_TEMPLATE}"
done
